﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    
    public Transform WeaponPj;
    public GameObject bulletPrefab;
    public float timer = 0;


    void Update()
    {
        if (Input.GetKey(KeyCode.Z) && timer <= 0)
        {
            Shoot();
            
        }
        if (timer != 0 && timer > 0)
        {
            timer -= 0.1f;
        }
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, WeaponPj.position, WeaponPj.rotation);
        timer += 1.5f;
    }

}
